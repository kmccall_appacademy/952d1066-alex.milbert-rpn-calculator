class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus

    perform_operation(:+)
  end

  def minus
    
    perform_operation(:-)
  end

  def divide
    # second_operand = @stack.pop
    # first_operand = @stack.pop
    # result =  first_operand.to_f / second_operand
    # @stack << result
    perform_operation(:/)
  end

  def times
    # result = @stack.pop * @stack.pop
    # @stack << result
    perform_operation(:*)
  end


  def tokens(string)
    tokens = string.split
    symbs = [:*,:/,:+,:-]
    tokens.map! do |el|
      if symbs.include?(el.to_sym)
        el.to_sym
      else
        el.to_i
      end
    end
    tokens
  end

  def evaluate(string)
    tokens  = tokens(string)
    tokens.each do |element|

      case element
      when Integer
        push(element)
      else
        perform_operation(element)
      end
    end
    value
  end
  private

  def operation?(char)
    [:+, :-, :/, :*].include?(char.to_sym)
  end

  def perform_operation(operation_symbol)

    raise "calculator is empty" if @stack.size < 2

    second_operand = @stack.pop
    first_operand = @stack.pop

    case operation_symbol
    when :+
      @stack << first_operand + second_operand
    when :-
      @stack << first_operand - second_operand
    when :*
      @stack << first_operand * second_operand
    when :/
      @stack << first_operand.to_f / second_operand
    else
      @stack << first_operand
      @stack << second_operand
      raise "No such operation: #{operation_symbol}"
    end
  end

end
